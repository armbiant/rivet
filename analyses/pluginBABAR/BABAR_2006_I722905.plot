BEGIN PLOT /BABAR_2006_I722905/d01-x01-y01
Title=$K^-\pi^0$ mass distribution in $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{K^-\pi^0}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^0}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I722905/d02-x01-y01
Title=$K^+\pi^0$ mass distribution in $D^0\to K^+\pi^-\pi^0$
XLabel=$m^2_{K^+\pi^0}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^0}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
