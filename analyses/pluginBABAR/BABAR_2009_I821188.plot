BEGIN PLOT /BABAR_2009_I821188/d01-x01-y01
Title=$\pi^+\pi^-$ mass for $B^0\to K^0_S\pi^+\pi^-$ (+ve helicity)
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I821188/d01-x01-y02
Title=$\pi^+\pi^-$ mass for $B^0\to K^0_S\pi^+\pi^-$ (-ve helicity)
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I821188/d02-x01-y01
Title=$K^0_S\pi$ mass for $B^0\to K^0_S\pi^+\pi^-$ (+ve helicity)
XLabel=$m_{K^0_S\pi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I821188/d02-x01-y02
Title=$K^0_S\pi$ mass for $B^0\to K^0_S\pi^+\pi^-$ (-ve helicity)
XLabel=$m_{K^0_S\pi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
