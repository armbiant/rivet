BEGIN PLOT /BABAR_2013_I1206605/d01-x01-y01
Title=$K^+\pi^+$ mass distribution in $D^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^+\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1206605/d01-x01-y02
Title=$K^+K^+$ mass distribution in $D^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^+K^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1206605/d01-x01-y03
Title=$K^-\pi^+$ mass distribution in $D^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1206605/dalitz
Title=Dalitz plot for $D^+\to K^+K^-\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^+K^+}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{K^+K^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
