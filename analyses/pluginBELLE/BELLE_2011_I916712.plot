BEGIN PLOT /BELLE_2011_I916712/d01-x01-y01
Title=$\cos\theta_X$ distribution for $B\to KX(3872)(\to J\psi\pi^+\pi^-)$
XLabel=$|\cos\theta_X|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|\cos\theta_X|$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2011_I916712/d02-x01-y01
Title=$\cos\chi$ distribution for $B\to KX(3872)(\to J\psi\pi^+\pi^-)$
XLabel=$|\cos\chi|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|\cos\chi|$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2011_I916712/d03-x01-y01
Title=$\cos\theta_\ell$ distribution for $B\to KX(3872)(\to J\psi\pi^+\pi^-)$
XLabel=$|\cos\theta_\ell|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|\cos\theta_\ell|$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2011_I916712/d04-x01-y01
Title=$\pi^+\pi^-$ mass distribution for $X(3872)\to J\psi\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
