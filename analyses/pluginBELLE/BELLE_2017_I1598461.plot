BEGIN PLOT /BELLE_2017_I1598461/d01-x01-y01
Title=Differential branching ratio w.r.t. $K^+K^-$ mass for $B^+\to K^+K^-\pi^+$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{K^+K^-}\times10^7$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1598461/d01-x01-y02
Title=$A_{CP}$ w.r.t. $K^+K^-$ mass for $B^+\to K^+K^-\pi^+$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$A_{CP}$
LogY=0
END PLOT
