BEGIN PLOT /BELLE_2019_I1693396/d01-x01
LogY=0
XLabel=$w$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}w$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d02
LogY=0
XLabel=$\cos\theta_\ell$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\ell$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d03
LogY=0
XLabel=$\cos\theta_v$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_v$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d04
LogY=0
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$ [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d01-x01-y01
Title=$w$ distribution for $B^0\to D^{*-} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d01-x01-y02
Title=$w$ distribution for $B^0\to D^{*-} \mu^+\nu_\mu$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d02-x01-y01
Title=$\cos\theta_\ell$ distribution for $B^0\to D^{*-} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d02-x01-y02
Title=$\cos\theta_\ell$ distribution for $B^0\to D^{*-} \mu^+\nu_\mu$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d03-x01-y01
Title=$\cos\theta_v$ distribution for $B^0\to D^{*-} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d03-x01-y02
Title=$\cos\theta_v$ distribution for $B^0\to D^{*-} \mu^+\nu_\mu$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d04-x01-y01
Title=$\chi$ distribution for $B^0\to D^{*-} e^+\nu_e$
END PLOT
BEGIN PLOT /BELLE_2019_I1693396/d04-x01-y02
Title=$\chi$ distribution for $B^0\to D^{*-} \mu^+\nu_\mu$
END PLOT
