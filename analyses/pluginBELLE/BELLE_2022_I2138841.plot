BEGIN PLOT /BELLE_2022_I2138841
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d0[1,2]
YMin=-1
YMax=1
END PLOT

BEGIN PLOT /BELLE_2022_I2138841/d01-x01-y01
Title=$\alpha$ for $\Lambda_c^+\to\Lambda^0K^+$ and $\bar\Lambda_c^-\to\bar\Lambda^0K^-$
YLabel=$\alpha_{\Lambda^0}\times\alpha_{\Lambda_c\to\Lambda^0K^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d01-x02-y01
Title=$\alpha$ for $\Lambda_c^+\to\Lambda^0\pi^+$ and $\bar\Lambda_c^-\to\bar\Lambda^0\pi^-$
YLabel=$\alpha_{\Lambda^0}\times\alpha_{\Lambda_c\to\Lambda^0\pi^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d01-x03-y01
Title=$\alpha$ for $\Lambda_c^+\to\Sigma^0K^+$ and $\bar\Lambda_c^-\to\bar\Sigma^0K^-$
YLabel=$\alpha_{\Sigma^0}\times\alpha_{\Lambda_c\to\Sigma^0K^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d01-x04-y01
Title=$\alpha$ for $\Lambda_c^+\to\Sigma^0\pi^+$ and $\bar\Lambda_c^-\to\bar\Sigma^0\pi^-$
YLabel=$\alpha_{\Sigma^0}\times\alpha_{\Lambda_c\to\Sigma^0\pi^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d01-x01-y02
Title=$\alpha_{\Lambda_c\to\Lambda^0K^+}$ for $\Lambda_c^+\to\Lambda^0K^+$ and $\bar\Lambda_c^-\to\bar\Lambda^0K^-$
YLabel=$\alpha_{\Lambda_c\to\Lambda^0K^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d01-x02-y02
Title=$\alpha_{\Lambda_c\to\Lambda^0\pi^+}$ for $\Lambda_c^+\to\Lambda^0\pi^+$ and $\bar\Lambda_c^-\to\bar\Lambda^0\pi^-$
YLabel=$\alpha_{\Lambda_c\to\Lambda^0\pi^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d01-x03-y02
Title=$\alpha_{\Lambda_c\to\Sigma^0K^+}$ for $\Lambda_c^+\to\Sigma^0K^+$ and $\bar\Lambda_c^-\to\bar\Sigma^0K^-$
YLabel=$\alpha_{\Lambda_c\to\Sigma^0K^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d01-x04-y02
Title=$\alpha_{\Lambda_c\to\Sigma^0\pi^+}$ for $\Lambda_c^+\to\Sigma^0\pi^+$ and $\bar\Lambda_c^-\to\bar\Sigma^0\pi^-$
YLabel=$\alpha_{\Lambda_c\to\Sigma^0\pi^+}$
END PLOT




BEGIN PLOT /BELLE_2022_I2138841/d02-x01-y01
Title=$\alpha$ for $\Lambda_c^+\to\Lambda^0K^+$
YLabel=$\alpha_{\Lambda^0}\times\alpha_{\Lambda_c\to\Lambda^0K^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x02-y01
Title=$\alpha$ for $\Lambda_c^+\to\Lambda^0\pi^+$
YLabel=$\alpha_{\Lambda^0}\times\alpha_{\Lambda_c\to\Lambda^0\pi^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x03-y01
Title=$\alpha$ for $\Lambda_c^+\to\Sigma^0K^+$
YLabel=$\alpha_{\Sigma^0}\times\alpha_{\Lambda_c\to\Sigma^0K^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x04-y01
Title=$\alpha$ for $\Lambda_c^+\to\Sigma^0\pi^+$
YLabel=$\alpha_{\Sigma^0}\times\alpha_{\Lambda_c\to\Sigma^0\pi^+}$
END PLOT

BEGIN PLOT /BELLE_2022_I2138841/d02-x01-y02
Title=$\alpha$ for $\bar\Lambda_c^-\to\bar\Lambda^0K^+$
YLabel=$\alpha_{\bar\Lambda^0}\times\alpha_{\bar\Lambda_c\to\bar\Lambda^0K^-}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x02-y02
Title=$\alpha$ for $\bar\Lambda_c^-\to\bar\Lambda^0\pi^+$
YLabel=$\alpha_{\bar\Lambda^0}\times\alpha_{\bar\Lambda_c\to\bar\Lambda^0\pi^-}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x03-y02
Title=$\alpha$ for $\bar\Lambda_c^-\to\bar\Sigma^0K^+$
YLabel=$\alpha_{\bar\Sigma^0}\times\alpha_{\bar\Lambda_c\to\bar\Sigma^0K^-}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x04-y02
Title=$\alpha$ for $\bar\Lambda_c^-\to\bar\Sigma^0\pi^+$
YLabel=$\alpha_{\bar\Sigma^0}\times\alpha_{\\barLambda_c\to\bar\Sigma^0\pi^-}$
END PLOT


BEGIN PLOT /BELLE_2022_I2138841/d02-x01-y03
Title=$\alpha_{\Lambda_c\to\Lambda^0K^+}$ for $\Lambda_c^+\to\Lambda^0K^+$
YLabel=$\alpha_{\Lambda_c\to\Lambda^0K^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x02-y03
Title=$\alpha_{\Lambda_c\to\Lambda^0\pi^+}$ for $\Lambda_c^+\to\Lambda^0\pi^+$
YLabel=$\alpha_{\Lambda_c\to\Lambda^0\pi^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x03-y03
Title=$\alpha_{\Lambda_c\to\Sigma^0K^+}$ for $\Lambda_c^+\to\Sigma^0K^+$
YLabel=$\alpha_{\Lambda_c\to\Sigma^0K^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x04-y03
Title=$\alpha_{\Lambda_c\to\Sigma^0\pi^+}$ for $\Lambda_c^+\to\Sigma^0\pi^+$
YLabel=$\alpha_{\Lambda_c\to\Sigma^0\pi^+}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x01-y04
Title=$\alpha_{\bar\Lambda_c\to\bar\Lambda^0K^-}$ for $\bar\Lambda_c^-\to\bar\Lambda^0K^-$
YLabel=$\alpha_{\bar\Lambda_c\to\bar\Lambda^0K^-}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x02-y04
Title=$\alpha_{\bar\Lambda_c\to\bar\Lambda^0\pi^-}$ for $\bar\Lambda_c^-\to\bar\Lambda^0\pi^-$
YLabel=$\alpha_{\bar\Lambda_c\to\bar\Lambda^0\pi^-}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x03-y04
Title=$\alpha_{\bar\Lambda_c\to\bar\Sigma^0K^-}$ for $\bar\Lambda_c^-\to\bar\Sigma^0K^-$
YLabel=$\alpha_{\bar\Lambda_c\to\bar\Sigma^0K^-}$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d02-x04-y04
Title=$\alpha_{\bar\Lambda_c\to\bar\Sigma^0\pi^-}$ for $\bar\Lambda_c^-\to\bar\Sigma^0\pi^-$
YLabel=$\alpha_{\bar\Lambda_c\to\\barSigma^0\pi^-}$
END PLOT

BEGIN PLOT /BELLE_2022_I2138841/d03-x01-y01
Title=$\cos\theta$ for $\Lambda_c^+\to\Lambda^0K^+$ and $\bar\Lambda_c^-\to\bar\Lambda^0K^+$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d03-x01-y02
Title=$\cos\theta$ for $\Lambda_c^+\to\Lambda^0\pi^+$ and $\bar\Lambda_c^-\to\bar\Lambda^0\pi^+$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT

BEGIN PLOT /BELLE_2022_I2138841/d04-x01-y01
Title=$\cos\theta$ for $\Lambda_c^+\to\Lambda^0K^+$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d04-x01-y02
Title=$\cos\theta$ for $\bar\Lambda_c^-\to\bar\Lambda^0K^-$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d04-x01-y03
Title=$\cos\theta$ for $\Lambda_c^+\to\Lambda^0\pi^+$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT
BEGIN PLOT /BELLE_2022_I2138841/d04-x01-y04
Title=$\cos\theta$ for $\bar\Lambda_c^-\to\bar\Lambda^0\pi^-$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
END PLOT
