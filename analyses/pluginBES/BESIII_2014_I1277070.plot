BEGIN PLOT /BESIII_2014_I1277070/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^+\to K_S^0\pi^+\pi^0$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1277070/d01-x01-y02
Title=$K^0_S\pi^0$ mass distribution in $D^+\to K_S^0\pi^+\pi^0$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1277070/d01-x01-y03
Title=$K^0_S\pi^+$ mass distribution in $D^+\to K_S^0\pi^+\pi^0$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1277070/dalitz
Title=Dalitz plot for $D^+\to K_S^0\pi^+\pi^0$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-2}$]
YLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\K_S^0\pi^0}/{\rm d}m^2_{\pi^+\pi^0}$ [$\rm{GeV}^{-4}$]
END PLOT
