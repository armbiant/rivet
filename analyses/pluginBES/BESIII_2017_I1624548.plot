BEGIN PLOT /BESIII_2017_I1624548/d01-x01-y01
Title=$\cos\theta_1$ for $\gamma_1$ in $\psi(2S)\to\gamma_1\chi_{c2}$
XLabel=$\cos\theta_1$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_1$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1624548/d01-x01-y02
Title=$\cos\theta_2$ for $\gamma_2$ in $\psi(2S)\to\gamma_1\chi_{c2}$, $\chi_{c2}\to\gamma\gamma$
XLabel=$\cos\theta_2$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_2$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1624548/d01-x01-y03
Title=$\phi_2$ for $\gamma_2$ in $\psi(2S)\to\gamma_1\chi_{c2}$, $\chi_{c2}\to\gamma\gamma$
XLabel=$\phi_2$ [rad]
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\phi_2$ [$\mathrm{rad}^{-1}$]
LogY=0
END PLOT
