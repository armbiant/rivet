BEGIN PLOT /BESIII_2018_I1664315
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{e^+e^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2018_I1664315/d01-x01-y01
Title=$e^+e^-$ mass distribution in $\psi(2S)\to\eta^\prime e^+e^-$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1664315/d01-x01-y02
Title=$e^+e^-$ mass distribution in $\psi(2S)\to\eta^\prime e^+e^-$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
END PLOT
BEGIN PLOT /BESIII_2018_I1664315/d01-x01-y03
Title=$e^+e^-$ mass distribution in $\psi(2S)\to\eta^\prime e^+e^-$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1664315/d01-x01-y04
Title=$e^+e^-$ mass distribution in $\psi(2S)\to\eta^\prime e^+e^-$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
END PLOT
