BEGIN PLOT /BESIII_2018_I1703033/d01-x01-y01
Title=$z$ distribution for $\omega\to\pi^+\pi^-\pi^0$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}z$
XLabel=$z$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1703033/d01-x01-y02
Title=$\phi$ distribution for $\omega\to\pi^+\pi^-\pi^0$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\phi$
XLabel=$\phi$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1703033/d01-x01-y03
Title=$\cos\theta_{\pi^+\pi^0}$ distribution for $\omega\to\pi^+\pi^-\pi^0$
XLabel=$\cos\theta_{\pi^+\pi^0}$ 
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_{\pi^+\pi^0}$ 
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1703033/d01-x01-y04
Title=$|p_{\pi^0}|$ distribution for $\omega\to\pi^+\pi^-\pi^0$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}|p_{\pi^0}|$ $[\mathrm{GeV}^{-1}]$
XLabel=$|p_{\pi^0}|$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1703033/dalitz
Title=Dalitz plot for $\omega\to\pi^+\pi^-\pi^0$
XLabel=$x$
YLabel=$y$
ZLabel=$1/\Gamma{\rm d}\Gamma/{\rm d}x/{\rm d}y$
LogY=0
END PLOT
