Name: BESIII_2021_I1854317
Year: 2021
Summary: Dalitz decay of $D^+_s\to K^0_S\pi^+\pi^0$
Experiment: BESIII
Collider: 
InspireID: 1854317
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 06 (2021) 181
RunInfo: Any process producing D_s+ mesons
Description:
  'Measurement of the mass distributions in the decay $D^+_s\to K^0_S\pi^+\pi^0$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events using model in the paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021xox
BibTeX: '@article{BESIII:2021xox,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Amplitude analysis and branching-fraction measurement of $ {D}_s^{+}\to {K}_S^0{\pi}^{+}{\pi}^0 $}",
    eprint = "2103.15098",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1007/JHEP06(2021)181",
    journal = "JHEP",
    volume = "06",
    pages = "181",
    year = "2021"
}
'
