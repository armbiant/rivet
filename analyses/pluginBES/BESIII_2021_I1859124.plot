BEGIN PLOT /BESIII_2021_I1859124/d01-x01-y01
Title=$K^+\pi^0$  mass distribution in $D^+\to K^+K^0_S\pi^0$
XLabel=$m_{K^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1859124/d01-x01-y02
Title=$K^0_S\pi^0$  mass distribution in $D^+\to K^+K^0_S\pi^0$
XLabel=$m_{K^0_S\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1859124/d01-x01-y03
Title=$K^+K^0_S$  mass distribution in $D^+\to K^+K^0_S\pi^0$
XLabel=$m_{K^+K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1859124/dalitz
Title=Dalitz plot for $D^+\to K^+K^0_S\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^0}/{\rm d}m^2_{K^0_S\pi^0}$ [$\rm{GeV}^{-4}$]
END PLOT
