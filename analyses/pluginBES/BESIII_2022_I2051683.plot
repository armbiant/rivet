BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y01
Title=$K^+K^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^+K^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y02
Title=$K^+K^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^+K^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y03
Title=$\pi^+_1\pi^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+_1\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+_1\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y04
Title=$\pi^+_2\pi^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+_2\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+_2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y05
Title=$K^+K^-\pi^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^+K^-\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y06
Title=$K^-\pi^+_2\pi^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^-\pi^+_2\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y07
Title=$K^+\pi^+_1\pi^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^+\pi^+_1\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^+_1\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y08
Title=$\pi^+_1\pi^+_2\pi^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+_1\pi^+_2\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+_1\pi^+_2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y09
Title=$K^+K^-\pi^+_2\pi^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^+K^-\pi^+_2\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-\pi^+_2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2051683/d01-x01-y10
Title=$K^-\pi^+_1\pi^+_2\pi^-$ mass distribution in $D^+_s\to K^+K^-\pi^+\pi^+\pi^-$
XLabel=$m_{K^-\pi^+_1\pi^+_2\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+_1\pi^+_2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
