BEGIN PLOT /BESIII_2023_I2621481/d01-x01-y01
Title=$D^0\to pi^+\pi^+\pi^-+X$ branching ratio
YLabel=$\mathcal{B}(D^0\to pi^+\pi^+\pi^-+X)$   [$\%$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2621481/d01-x01-y02
Title=$D^+\to pi^+\pi^+\pi^-+X$ branching ratio
YLabel=$\mathcal{B}(D^+\to pi^+\pi^+\pi^-+X)$   [$\%$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2023_I2621481/d02-x01-y01
Title=$D^0\to pi^+\pi^+\pi^-+X$ branching ratio vs $m_{pi^+\pi^+\pi^-}$
XLabel=$m_{pi^+\pi^+\pi^-}$ [GeV]
YLabel=$\text{d}\mathcal{B}(D^0\to pi^+\pi^+\pi^-+X)/\text{d}m_{pi^+\pi^+\pi^-}$   [$\%/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2621481/d02-x01-y02
Title=$D^+\to pi^+\pi^+\pi^-+X$ branching ratio vs $m_{pi^+\pi^+\pi^-}$
XLabel=$m_{pi^+\pi^+\pi^-}$ [GeV]
YLabel=$\text{d}\mathcal{B}(D^+\to pi^+\pi^+\pi^-+X)/\text{d}m_{pi^+\pi^+\pi^-}$   [$\%/\text{GeV}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2023_I2621481/d03-x01-y01
Title=Harder $\pi^+$ momentum in $D^0\to pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^+}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2621481/d03-x02-y01
Title=Harder $\pi^+$ momentum in $D^+\to pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^+}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2621481/d03-x01-y02
Title=Softer $\pi^+$ momentum in $D^0\to pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^+}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2621481/d03-x02-y02
Title=Softer $\pi^+$ momentum in $D^+\to pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^+}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2621481/d03-x01-y03
Title=$\pi^-$ momentum in $D^0\to pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^-}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2621481/d03-x02-y03
Title=$\pi^-$ momentum in $D^+\to pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^-}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
