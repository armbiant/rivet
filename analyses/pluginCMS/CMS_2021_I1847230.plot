# BEGIN PLOT /CMS_2021_I1847230/d01-x01-y01
Title=CMS, 8 TeV, Three-jet pT3/pT2 for small-angle radiation
XLabel=$p_{T3}/p_{T2}$
YLabel=$\frac{1}{N} \frac{dN}{d(p_{T3}/p_{T2})}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d02-x01-y01
Title=CMS, 8 TeV, Three-jet pT3/pT2 for large-angle radiation
XLabel=$p_{T3}/p_{T2}$
YLabel=$\frac{1}{N} \frac{dN}{d(p_{T3}/p_{T2})}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d03-x01-y01
Title=CMS, 8 TeV, Three-jet DR23 for soft radiation
XLabel=$\Delta R_{23}$
YLabel=$\frac{1}{N} \frac{dN}{d\Delta R_{23}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d04-x01-y01
Title=CMS, 8 TeV, Three-jet DR23 for soft radiation
XLabel=$\Delta R_{23}$
YLabel=$\frac{1}{N} \frac{dN}{d\Delta R_{23}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d05-x01-y01
Title=CMS, 13 TeV, Three-jet pT3/pT2 for small-angle radiation
XLabel=$p_{T3}/p_{T2}$
YLabel=$\frac{1}{N} \frac{dN}{d(p_{T3}/p_{T2})}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d06-x01-y01
Title=CMS, 13 TeV, Three-jet pT3/pT2 for large-angle radiation
XLabel=$p_{T3}/p_{T2}$
YLabel=$\frac{1}{N} \frac{dN}{d(p_{T3}/p_{T2})}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d07-x01-y01
Title=CMS, 13 TeV, Three-jet DR23 for soft radiation
XLabel=$\Delta R_{23}$
YLabel=$\frac{1}{N} \frac{dN}{d\Delta R_{23}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d08-x01-y01
Title=CMS, 13 TeV, Three-jet DR23 for hard radiation
XLabel=$\Delta R_{23}$
YLabel=$\frac{1}{N} \frac{dN}{d\Delta R_{23}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d09-x01-y01
Title=CMS, 8 TeV, Z + two-jet pT3/pT2 for small-angle radiation
XLabel=$p_{T3}/p_{T2}$
YLabel=$\frac{N_{j3}}{N_{j2}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d10-x01-y01
Title=CMS, 8 TeV, Z + two-jet pT3/pT2 for large-angle radiation
XLabel=$p_{T3}/p_{T2}$
YLabel=$\frac{N_{j3}}{N_{j2}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d11-x01-y01
Title=CMS, 8 TeV, Z + two-jet DR23 for soft radiation
XLabel=$\Delta R_{23}$
YLabel=$\frac{N_{j3}}{N_{j2}}$
LogX=0
LogY=0
# END PLOT

# BEGIN PLOT /CMS_2021_I1847230/d12-x01-y01
Title=CMS, 8 TeV, Z + two-jet DR23 for hard radiation
XLabel=$\Delta R_{23}$
YLabel=$\frac{N_{j3}}{N_{j2}}$
LogX=0
LogY=0
# END PLOT

