BEGIN PLOT /KLOE_2013_I1186739/d01-x01-y01
Title=Differential $\eta\to \pi^+\pi^-\gamma$ decay
XLabel=$m_{\pi^+\pi^-}$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{MeV}^{-1}$]
LogY=0
END PLOT
