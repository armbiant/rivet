
# ... add more histograms as you need them ...
BEGIN PLOT/ZEUS_1999_I508906/d01-x01-y01
LogX=1
LogY=1
Title=ZEUS 1995
XLabel=$E^2/Q^2$
YLabel=$dsigma/d(E^2/Q^2)nb$
LegendXPos=0.2
LegendYPos=0.7
END PLOT