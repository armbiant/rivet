BEGIN PLOT /MARKII_1988_I246184/d01-x01-y01
Title=Aplanarity, $A$ (charged+neutral) (average)
XLabel=$A$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{A}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d02-x01-y01
Title=$Q_x$ (charged+neutral) (average)
XLabel=$Q_x$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{Q_x}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d03-x01-y01
Title=$Q_2-Q_1$ (charged+neutral) (average)
XLabel=$Q_2-Q_1$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{Q_2-Q_1}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d04-x01-y01
Title=Sphericity, $S$ (charged+neutral) (average)
XLabel=$S$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{S}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d05-x01-y01
Title=Thrust, $T$ (charged+neutral) (average)
XLabel=$T$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{T}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d06-x01-y01
Title=Thrust minor, $m$ (charged+neutral) (average)
XLabel=$m$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{m}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d07-x01-y01
Title=Oblateness, $M - m$ (charged+neutral) (average)
XLabel=$O$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{O}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d08-x01-y01
Title=Broad jet mass (charged+neutral) (average)
XLabel=$M_b^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_b^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d09-x01-y01
Title=Slim jet mass (charged+neutral) (average)
XLabel=$M_s^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_s^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d10-x01-y01
Title=Jet mass different (charged+neutral) (average)
XLabel=$M_d^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_d^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d11-x01-y01
Title=$p_T^2$ $\mathrm{GeV}^2$ w.r.t. sphericity axes (charged) (average)
XLabel=$p_\perp^2$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^2}$ [$\mathrm{GeV}^{-2}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d12-x01-y01
Title=$p_T$ GeV w.r.t. sphericity axes (charged)
XLabel=$p_\perp$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d13-x01-y01
Title=Out-plane $p_T$ out GeV w.r.t. sphericity axes (charged) (average)
XLabel=$p_\perp^\mathrm{out}$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{out}}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d14-x01-y01
Title=In-plane $p_T$ in GeV w.r.t. sphericity axes (charged) (average)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d15-x01-y01
Title=Scaled momentum, $x_p = |p|/|p_\mathrm{beam}|$ (charged) (average)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d16-x01-y01
Title=Rapidity w.r.t. rapidity axes, $y_T$ (charged) (average)
XLabel=$y_S$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d17-x01-y01
Title=Charged particle flow w.r.t. rapidity axes (charged) (average)
XLabel=$\theta$ [degrees]
YLabel=$1/N \, \mathrm{d}{n}/\mathrm{d}{\theta}$ [$\mathrm{deg}^{-1}$]
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d18-x01-y01
Title=Energy flow w.r.t. rapidity axes (charged+neutral) (average)
XLabel=$\theta$ [degrees]
YLabel=$1/N \, E\mathrm{d}{n}/\mathrm{d}{\theta}$ [$\mathrm{deg}^{-1}$]
FullRange=1
# END PLOT

BEGIN PLOT /MARKII_1988_I246184/d19-x01-y01
Title=Aplanarity, $A$ (charged+neutral) (PEP5)
XLabel=$A$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{A}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d20-x01-y01
Title=$Q_x$ (charged+neutral) (PEP5)
XLabel=$Q_x$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{Q_x}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d21-x01-y01
Title=$Q_2-Q_1$ (charged+neutral) (PEP5)
XLabel=$Q_2-Q_1$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{Q_2-Q_1}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d22-x01-y01
Title=Sphericity, $S$ (charged+neutral) (PEP5)
XLabel=$S$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{S}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d23-x01-y01
Title=Thrust, $T$ (charged+neutral) (PEP5)
XLabel=$T$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{T}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d24-x01-y01
Title=Thrust minor, $m$ (charged+neutral) (PEP5)
XLabel=$m$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{m}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d25-x01-y01
Title=Oblateness, $M - m$ (charged+neutral) (PEP5)
XLabel=$O$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{O}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d26-x01-y01
Title=Broad jet mass (charged+neutral) (PEP5)
XLabel=$M_b^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_b^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d27-x01-y01
Title=Slim jet mass (charged+neutral) (PEP5)
XLabel=$M_s^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_s^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d28-x01-y01
Title=Jet mass different (charged+neutral) (PEP5)
XLabel=$M_d^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_d^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d29-x01-y01
Title=$p_T^2$ $\mathrm{GeV}^2$ w.r.t. sphericity axes (charged) (PEP5)
XLabel=$p_\perp^2$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^2}$ [$\mathrm{GeV}^{-2}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d30-x01-y01
Title=$p_T$ GeV w.r.t. sphericity axes (charged)
XLabel=$p_\perp$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d31-x01-y01
Title=Out-plane $p_T$ out GeV w.r.t. sphericity axes (charged) (PEP5)
XLabel=$p_\perp^\mathrm{out}$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{out}}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d32-x01-y01
Title=In-plane $p_T$ in GeV w.r.t. sphericity axes (charged) (PEP5)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d33-x01-y01
Title=Scaled momentum, $x_p = |p|/|p_\mathrm{beam}|$ (charged) (PEP5)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d34-x01-y01
Title=Rapidity w.r.t. rapidity axes, $y_T$ (charged) (PEP5)
XLabel=$y_S$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d35-x01-y01
Title=Charged particle flow w.r.t. rapidity axes (charged) (PEP5)
XLabel=$\theta$ [degrees]
YLabel=$1/N \, \mathrm{d}{n}/\mathrm{d}{\theta}$ [$\mathrm{deg}^{-1}$]
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d36-x01-y01
Title=Energy flow w.r.t. rapidity axes (charged+neutral) (PEP5)
XLabel=$\theta$ [degrees]
YLabel=$1/N \, E\mathrm{d}{n}/\mathrm{d}{\theta}$ [$\mathrm{deg}^{-1}$]
FullRange=1
# END PLOT

BEGIN PLOT /MARKII_1988_I246184/d37-x01-y01
Title=Aplanarity, $A$ (charged+neutral) (upgrade)
XLabel=$A$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{A}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d38-x01-y01
Title=$Q_x$ (charged+neutral) (upgrade)
XLabel=$Q_x$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{Q_x}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d39-x01-y01
Title=$Q_2-Q_1$ (charged+neutral) (upgrade)
XLabel=$Q_2-Q_1$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{Q_2-Q_1}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d40-x01-y01
Title=Sphericity, $S$ (charged+neutral) (upgrade)
XLabel=$S$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{S}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d41-x01-y01
Title=Thrust, $T$ (charged+neutral) (upgrade)
XLabel=$T$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{T}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d42-x01-y01
Title=Thrust minor, $m$ (charged+neutral) (upgrade)
XLabel=$m$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{m}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d43-x01-y01
Title=Oblateness, $M - m$ (charged+neutral) (upgrade)
XLabel=$O$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{O}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d44-x01-y01
Title=Broad jet mass (charged+neutral) (upgrade)
XLabel=$M_b^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_b^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d45-x01-y01
Title=Slim jet mass (charged+neutral) (upgrade)
XLabel=$M_s^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_s^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d46-x01-y01
Title=Jet mass different (charged+neutral) (upgrade)
XLabel=$M_d^2/s$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}{(M_d^2/s)}$
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d47-x01-y01
Title=$p_T^2$ $\mathrm{GeV}^2$ w.r.t. sphericity axes (charged) (upgrade)
XLabel=$p_\perp^2$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^2}$ [$\mathrm{GeV}^{-2}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d48-x01-y01
Title=$p_T$ GeV w.r.t. sphericity axes (charged)
XLabel=$p_\perp$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d49-x01-y01
Title=Out-plane $p_T$ out GeV w.r.t. sphericity axes (charged) (upgrade)
XLabel=$p_\perp^\mathrm{out}$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{out}}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I246184/d50-x01-y01
Title=In-plane $p_T$ in GeV w.r.t. sphericity axes (charged) (upgrade)
XLabel=$p_\perp^\mathrm{in}$ / GeV
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{p_\perp^\mathrm{in}}$ [$\mathrm{GeV}^{-1}$]
FullRange=1
END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d51-x01-y01
Title=Scaled momentum, $x_p = |p|/|p_\mathrm{beam}|$ (charged) (upgrade)
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{x_p}$
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d52-x01-y01
Title=Rapidity w.r.t. rapidity axes, $y_T$ (charged) (upgrade)
XLabel=$y_S$
YLabel=$1/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{y_S}$
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d53-x01-y01
Title=Charged particle flow w.r.t. rapidity axes (charged) (upgrade)
XLabel=$\theta$ [degrees]
YLabel=$1/N \, \mathrm{d}{n}/\mathrm{d}{\theta}$ [$\mathrm{deg}^{-1}$]
FullRange=1
# END PLOT
# BEGIN PLOT /MARKII_1988_I246184/d54-x01-y01
Title=Energy flow w.r.t. rapidity axes (charged+neutral) (upgrade)
XLabel=$\theta$ [degrees]
YLabel=$1/N \, E\mathrm{d}{n}/\mathrm{d}{\theta}$ [$\mathrm{deg}^{-1}$]
FullRange=1
# END PLOT
